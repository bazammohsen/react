import React, {Component} from "react";
import "./App.scss";
import MainPage from "./Routes/MainPage/MainPage";
import {BrowserRouter as Router, Route} from "react-router-dom";
import LoginForgot from "./Routes/LoginForgot/LoginForgot";

class App extends Component {
    render() {
        return <Router>
            <Route path="/" exact render={(props) => <LoginForgot {...props}/>}/>
            <Route path="/home" render={(props) => <MainPage {...props}/>}/>
            {/*<Route exact path="/" component={LoginForgot}/>*/}
        </Router>;
    }
}

export default App;
