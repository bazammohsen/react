import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware, compose} from "redux";
import {Provider} from 'react-redux';
import Reducer from './Services/Store/Reducer'

const logger = Store => {
    return next => {
        return action => {
            console.log('[middleWare] dispatching', action)
            const result = next(action);
            console.log('[middleWare] next state', Store.getState())
            return result;
        }
    }
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/**
 *
 * can apply more middleware like const Store = createStore(Reducer, applyMiddleware(logger,sdfsd,sdfasdf,...));
 * middlewares will be executed in sequence{logger then sdfsd then ...}
 */
const Store = createStore(Reducer, composeEnhancers(applyMiddleware(logger)));

ReactDOM.render(<Provider store={Store}><App/></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
