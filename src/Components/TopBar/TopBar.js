import React from 'react'
import './TopBar.scss'
import Button from "@material-ui/core/Button";
import logOutImg from '../../assets/icons/log-out.png';
import setting from '../../assets/icons/settings.png';
import message from '../../assets/icons/message.png';
import notificationIcon from '../../assets/icons/notif.png';
import alarm from '../../assets/icons/alarm.png';
import list from '../../assets/icons/checklist.png';
import ToPersianNumber from "../../Common/Converters/ToPersianNumber";

const TopBar = (props) => {
    const NewMessageBadge = () => {
        let component;
        (props.data.Notifications.length) === 0 ?
            component = (null)
            :
            component = (
                <span className={'Badge'}>{ToPersianNumber.NumberConverter(props.data.Notifications.length)}</span>
            );
        return component;
    };
    const NewNotificationBadge = () => {
        let component;
        (props.data.Messages.length) === 0 ?
            component = (null)
            :
            component = (
                <span className={'Badge'}>{ToPersianNumber.NumberConverter(props.data.Messages.length)}</span>
            );
        return component;
    };
    const logOut = () => {
        localStorage.clear();
        props.history.push('/');
    }

    return (
        <div className={'TopBar'}>

            <div className={'TopIconHolder'}>
                <span>
                    <Button onClick={() => logOut()} className={'topBarButton'}>
                        <img src={logOutImg} alt=""/>
                    </Button>
                </span>
                <span>
                    <Button className={'topBarButton'}>
                        <img src={setting} alt=""/>
                    </Button>
                </span>
                <span>
                    <Button className={'topBarButton'}>
                        <NewMessageBadge/>
                        <img src={message} alt=""/>
                    </Button>
                </span>
                <span>
                    <Button className={'topBarButton'}>
                        <NewNotificationBadge/>
                        <img src={notificationIcon} alt=""/>
                    </Button>
                </span>
            </div>
            <div className={'todayHolder'}>
                  <span className={'dateText'}>
                   امروز
               </span>

                <span className={'dateHolder'}>
                    {ToPersianNumber.NumberConverter(props.data.today)}
                </span>


            </div>

            <div className={'additionalDataHolder'}>
                <div>

                    <span className={'text'}>باقیمانده تا ویزیت بعدی</span>
                    <span className={'blueText'}>مقدار</span>

                    <span><img src={alarm} alt=""/></span>

                </div>

                <div className={'margined'}>

                    <span className={'text'}>ویزیت های پیش رو</span>
                    <span className={'blueText'}>مقدار</span>
                    <span><img src={list} alt=""/></span>

                </div>

            </div>


        </div>
    );
};
export default TopBar;
