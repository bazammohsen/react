import React from 'react';
import './WhiteContainer.scss'
import Button from "@material-ui/core/es/Button/Button";

const WhiteContainer = (props) => {
    return (
        <div className='WhiteContainer'>
            <div className='WhiteContainerHeader'>
                <p className='WhiteContainerTitle'>{props.title}</p>
                <Button className='TransparentButton RefreshButton'
                        onClick={() => props.reload()}>{props.buttonTitle}</Button>
            </div>
            <div className="WhiteContainerBody">
                {props.body}
            </div>
        </div>
    );
}
export default WhiteContainer;
