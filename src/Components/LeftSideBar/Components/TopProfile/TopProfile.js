import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import './TopProfile.scss'
import MyProfileMenu from "./Components/MyProfileMenu";

const TopProfile =(props)=> {
   const profileImage = props.profileImage;
    return (
       <div>
        <div className="TopProfile_component">
            <span className="avatarSpan"><Avatar alt="Remy Sharp" src={profileImage} className="bigAvatar"  /></span>
            <span><p>دکتر {props.doctorName}</p></span>
        </div>
           <div className="status">
           <MyProfileMenu title={props.onlineStatus}></MyProfileMenu>
           </div>
       </div>
);
}
export default TopProfile;