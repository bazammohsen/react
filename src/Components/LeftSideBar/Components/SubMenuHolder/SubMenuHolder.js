import React, {useState} from "react";
import './SubMenuHolder.scss'
import SubMenuContent from "./Components/SubMenuContent/SumMenuContent";

const SubMenuHolder = React.memo(props => {
    const [SubsState, setSubsState] = useState(
        {showSubs: 'show'}
    );
    const ShowHideHandler = () => {
        if (SubsState.showSubs === 'hide') {
            setSubsState(
                {showSubs: 'show'}
            )
        } else {
            setSubsState(
                {showSubs: 'hide'}
            )
        }
    };
    const MakeContents = () => {
        let co = props.contents.map((content, key) => {
            return <SubMenuContent setpage={props.setpage} key={key} myKey={key} myId={(props.preId + '' + key)}
                                   title={content}/>
        });
        return (
            co
        );
    };
    return (
        <div className="SubMenuHolder">
            <p onClick={ShowHideHandler}>
                <span><img src={props.image} alt=""/></span>
                <span className="SubMenuHolderTitle">{props.title}</span>

            </p>
            <div className={"foldable " + SubsState.showSubs}>
                <MakeContents/>
            </div>
        </div>
    );
})

export default SubMenuHolder;
