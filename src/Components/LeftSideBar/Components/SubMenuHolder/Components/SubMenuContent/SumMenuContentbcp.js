import React ,{useState}from 'react'
import Button from "@material-ui/core/Button";
import './SubMenuContent.scss'
const SubMenuContent =props=>{
const [btnState,btnStateChanger]=useState(
    {
        blue:false
    }
);
    const btnStateHandler=()=>{
   let elements= document.getElementsByClassName('active');
if(elements.length>0) {
    elements[0].classList.remove('active')
}
        btnStateChanger(
            {blue:true}
        )

    };

    return(
        <Button  onClick={btnStateHandler} className={"whiteText "+ (btnState.blue===true?'active':'null')}>
            {props.title}
        </Button>
    );
};
export default SubMenuContent;