import React, {Component} from 'react'
import Button from "@material-ui/core/Button";
import './SubMenuContent.scss'
class  SubMenuContent extends Component{

constructor(props){
super(props);

this.state=
    {
        class:'whiteText',
        blue:false
    };
    this.elementRef = React.createRef()
}
  btnStateHandler ()  {
localStorage.selectedButton=this.props.myId;
          this.props.setpage(this.props.title);
    };
    render()
    {
    return(
        <Button id={this.props.myId} ref={this.elementRef} onClick={this.btnStateHandler.bind(this)} className={'whiteText'}>
            {this.props.title}
        </Button>
    );
    }

};
export default SubMenuContent;
