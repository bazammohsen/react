import "./LestSideBar.scss";
import React from "react";
import SubMenuHolder from "./Components/SubMenuHolder/SubMenuHolder";
import TopProfile from "./Components/TopProfile/TopProfile";
import dashboard from '../../assets/icons/dashboard.png';
import visits from '../../assets/icons/visits.png';
import patients from '../../assets/icons/persons.png';
import drugs from '../../assets/icons/drugs.png';
import reports from '../../assets/icons/chart.png'


const LeftSideBar = props => {

    return (
        <div className="wholeContainer">
            <div className="TopProfile">
                <TopProfile onlineStatus="آنلاین" profileImage='http://uupload.ir/files/7bz9_doctor.png'
                            doctorName="علی گرامیان راد"/>
            </div>
            <div className="LeftSideContainer">
                <SubMenuHolder setpage={props.setPage} title="داشبورد"
                               image={dashboard}
                               contents={['صفحه اصلی', 'پروفایل شما', 'تقویم کاری']}
                               preId='dashboard'
                >
                </SubMenuHolder>
                <SubMenuHolder setpage={props.setPage} title="ویزیت ها"
                               image={visits}
                               contents={['ویزیت های امروز', 'ویزیت های آینده', 'ویزیت های گذشته']}
                               preId='visits'
                >
                </SubMenuHolder>
                <SubMenuHolder setpage={props.setPage} title="بیماران"
                               image={patients}
                               contents={['لیست بیماران']}
                               preId='persons'
                >
                </SubMenuHolder>
                <SubMenuHolder setpage={props.setPage} title="داروها"
                               image={drugs}
                               contents={['لیست داروها']}
                               preId='drugs'
                >
                </SubMenuHolder>
                <SubMenuHolder setpage={props.setPage} title="گزارشات"
                               image={reports}
                               contents={['گزارش ویزیت ها', 'لیست ویزیت شده ها']}
                               preId='chart'
                >
                </SubMenuHolder>
            </div>
        </div>);
};
export default LeftSideBar;
