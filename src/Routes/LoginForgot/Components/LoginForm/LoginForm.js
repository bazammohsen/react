import React, {useState} from 'react';
import useForm from "./useForm";
import validate from './LoginFormValidationRules';
import {Button} from "@material-ui/core";
import loading from '../../../../assets/gif/loader.gif'
import stethoscope from "../../../../assets/icons/stethoscope.png";

const LoginForm = (props) => {
    const {
        values,
        errors,
        handleChange,
        handleSubmit,
    } = useForm(login, validate);
    const [Loading, LoadingChanger] = useState({
        loadingStatus: 'notLoading'
    })

    function login() {
        LoadingChanger({
            loadingStatus: 'isLoading'
        });
        console.log('No errors, submit callback called!');
        setTimeout(() => {
            LoadingChanger({
                loadingStatus: 'isLoading'
            });
            props.history.push('/home')
        }, 2000)
    }


    return (
        <div className="section is-fullheight">
            <div className="container">
                <div className="column is-4 is-offset-4">
                    <div className="box">
                        <div className={'imageHolder'}>
                            <img src={stethoscope} alt=""/>
                        </div>
                        <div className={'LoginHeader'}><p>ورود به سامانه</p></div>
                        <form onSubmit={handleSubmit} noValidate>
                            <div className="field">
                                <label className="label">نام کاربری</label>
                                <div className="control">
                                    <input autoComplete="off" className={`input ${errors.email && 'is-danger'}`}
                                           type="number" name="email" onChange={handleChange} value={values.email || ''}
                                           required/>
                                    {errors.email && (
                                        <p className="help is-danger">{errors.email}</p>
                                    )}
                                </div>
                            </div>
                            <div className="field">
                                <label className="label pass">رمز ورود</label>
                                <div className="control">
                                    <input className={`input ${errors.password && 'is-danger'}`} type="password"
                                           name="password" onChange={handleChange} value={values.password || ''}
                                           required/>
                                </div>
                                {errors.password && (
                                    <p className="help is-danger">{errors.password}</p>
                                )}
                            </div>
                            <Button type="submit" className={"submitButton " + Loading.loadingStatus}><p>ورود</p><img
                                src={loading}
                                alt=""/>
                            </Button>
                        </form>
                        <div className={'forgotQuestion'}>
                            <p onClick={() => props.ChangeToOther()}>رمز عبور خود را فراموش کرده
                                اید؟</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginForm;
