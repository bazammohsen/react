export default function validate(values) {
    let errors = {};
    if (!values.email) {
        errors.email = 'وارد کردن نام کاربری الزامی است';
        // } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    } else if ((values.email + '').length < 10 || (values.email + '').length < 11) {
        errors.email = 'شماره تلفن نامعتبر';
    }
    if (!values.password) {
        errors.password = 'وارد کردن رمز ورود الزامی است';
    } else if (values.password.length < 8) {
        errors.password = 'رمز ورود باید بیشتر از 8 کاراکتر باشد';
    }
    return errors;
};
