import React, {Component} from 'react'
import './LoginForgot.scss'
import Button from "@material-ui/core/Button";
import stethoscope from '../../assets/icons/stethoscope.png'
import LoginForm from "./Components/LoginForm/LoginForm";

class LoginForgot extends Component {
    constructor(props) {
        super(props);
        this.state = {CurrentPage: 'Login', isLoading: 'notLoading'};
        console.log('login constructor')

    }

    componentWillMount() {
        console.log('login componentWillMount')
    }

    submitForgot(e) {
        e.preventDefault();
        console.log(e);
    }
    
    viewForm() {
        return (
            this.state.CurrentPage === 'Login' ? (
                <LoginForm {...this.props} ChangeToOther={() => this.setState({CurrentPage: 'Forgot'})}/>
            ) : (<div>
                <div className={'LoginHeader'}><p>فراموشی کلمه عبور</p></div>
                <div className={'imageHolder'}>
                    <img src={stethoscope} alt=""/>
                </div>
                <form onSubmit={this.submitForgot.bind(this)}>
                    <input type="text" placeholder={'تلفن'}/>
                    <Button className={'submitButton'} type={'submit'}>بازیابی کلمه عبور</Button>
                </form>
                <div className={'forgotQuestion'}>
                    <p onClick={() => this.setState({CurrentPage: 'Login'})}>ورود</p>
                </div>
            </div>)
        )
    }

    render() {
        return (
            <div className={'LoginForgot'}>
                {this.viewForm()}
            </div>
        );
    }
}

export default LoginForgot;
