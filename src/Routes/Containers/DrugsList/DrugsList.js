import React, {useEffect, useState} from "react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import {AgGridReact} from "ag-grid-react";
import "./DrugsList.scss";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import cancel from '../../../assets/icons/red_cancel.png';
import plus from '../../../assets/icons/plus.png';

const DrugsList = props => {
    const [DrugListState, DrugListStateChanger] = useState({
        DrugListGridState: {
            columnDefs: [],
            rowData: []
        },
        addDrug: false
    });

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
            .then(result => result.json())
            .then(rowData =>
                DrugListStateChanger({
                    DrugListGridState: {
                        columnDefs: [
                            {
                                headerName: "id",
                                field: "postId"
                            },
                            {
                                headerName: "نام",
                                field: "name"
                            },
                            {
                                headerName: "قیمت",
                                field: "id"
                            },
                            {
                                headerName: "توضیحات",
                                field: "body",
                                width: 500
                            }
                        ],
                        rowData: rowData
                    },
                    addDrug: false
                })
            );
    }, []);
    const filterDrugs = e => {
        console.log(e);
    };
    const addNewDrug = () => {
        document.querySelectorAll(".ag-center-cols-container")[0].style.top =
            "35px";
        DrugListStateChanger({
            DrugListGridState: DrugListState.DrugListGridState,
            addDrug: true
        });
    };
    const CancelAddDrugHandler = () => {
        document.querySelectorAll(".ag-center-cols-container")[0].style.top =
            "0px";
        DrugListStateChanger({
            DrugListGridState: DrugListState.DrugListGridState,
            addDrug: false
        });
    };
    const addDrugHandler = () => {
        console.log('handle add drug')
    };

    const addDrugPart = () => {
        let component;
        DrugListState.addDrug
            ? (component = (
                <div style={{position: "absolute", zIndex: "2", top: "87px"}}>
                    <Grid container className={"addDrugContainer"} spacing={16}>
                        <Grid xs={12} item className={'nopadding nomargin S12'}>

                            <Grid item xs={3} className="nopadding nomargin">
                                <input type="text" className="greyInput"/>
                            </Grid>
                            <Grid item xs={3} style={{marginRight: '-10px'}} className="nopadding nomargin">
                                <input type="text" className="greyInput"/>
                            </Grid>
                            <Grid
                                item
                                xs={1}
                                className="nopadding nomargin">
                                <input style={{marginRight: '-10px'}} type="text" className="greyInput"/>
                            </Grid>
                            <Grid item xs={2} className="nopadding nomargin">
                                <input type="text" className="greyInput"/>
                            </Grid>
                            <Grid
                                item
                                xs={2}
                                className="nopadding nomargin">
                                <input type="text" className="greyInput"/>
                            </Grid>
                            <Grid item xs={1} style={{marginTop: '15px'}} className="nopadding nomargin">
                                <span><img onClick={addDrugHandler} style={{
                                    cursor: 'pointer',
                                    maxWidth: '25px',
                                    marginRight: '5px',
                                    marginLeft: '20px'
                                }} src={plus} alt=""/></span>
                                <span><img onClick={CancelAddDrugHandler} style={{cursor: 'pointer', maxWidth: '25px'}}
                                           src={cancel}
                                           alt=""/></span>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            ))
            : (component = null);
        return component;
    };
    return (
        <div className="DrugsList">
            {addDrugPart()}
            <Grid container spacing={16}>
                <Grid item container xs={12} spacing={16}>
                    <Grid item xs={2}>
                        <Button onClick={addNewDrug} className={"addDrug"}>
                            <p>
                                <span>+</span>افزودن داروی جدید
                            </p>
                        </Button>
                    </Grid>
                    <Grid item xs={6}/>
                    <Grid item xs={4}>
                        <input
                            onChange={filterDrugs}
                            className={"searchInput drugSearchInput"}
                            type="text"
                            placeholder={"جستجوی دارو"}
                        />
                        <Button
                            disableTouchRipple={true}
                            className={"searchButtonForInput drugSearchButton"}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <div
                className="ag-theme-balham"
                style={{
                    marginTop: "0px",
                    height: "450px",
                    width: "100%"
                }}
            >
                <AgGridReact
                    suppressAnimationFrame={false}
                    filter={true}
                    sortable={true}
                    enableRtl={true}
                    animateRows={true}
                    columnDefs={DrugListState.DrugListGridState.columnDefs}
                    rowData={DrugListState.DrugListGridState.rowData}
                />
            </div>
        </div>
    );
};
export default DrugsList;
