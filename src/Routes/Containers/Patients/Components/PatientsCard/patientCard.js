import React, {useState} from 'react'
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import './PAtientsCard.scss';

const PatientCard = (props) => {
    console.log(props);
    const [cardState, cardStateChanger] = useState({
        properties: props.data
    })
    return (
        <Grid item xs={4} sm={4}>
            <Card className={'patientCard'} key={cardState.key}>
                <div className={'leftDiv'}>
                    <div className={'profilePicHolder'}>

                        <img src="http://uupload.ir/files/bp5l_recovered.png" alt=""/>

                    </div>
                    <Button className={'fz10 blackText'}>ویرایش اطلاعات</Button>
                </div>
                <p>
                    <span className={'blackSpan'}>نام: </span><span
                    className={'blueSpan bigSpan'}>{cardState.properties.name}</span>
                </p>
                <p>
                    <span className={'blackSpan'}>جنسیت: </span><span
                    className={'blueSpan'}>{cardState.properties.name}</span>
                </p>
                <p>
                    <span className={'blackSpan'}>سن: </span><span
                    className={'blueSpan'}>{cardState.properties.name}</span>
                </p>
                <p>
                    <span className={'blackSpan'}>آخرین ویزیت: </span><span
                    className={'blueSpan'}>{cardState.properties.name}</span>
                </p>
                <p>
                    <span className={'blackSpan'}>آخرین وضعیت: </span><span
                    className={'blueSpan'}>{cardState.properties.name}</span>
                </p>
                <Divider variant="middle"/>
                <div>
                    <div className="s4 rightDiv blackTextContainer">
                        <Button className={'fz10 blackText'}>
                                          <span>
                          <img className={'separatedImage'} src="http://uupload.ir/files/2rje_file.png" alt=""/>
                      </span>
                            <span className={'blackText'}>
                          پرونده پزشکی
                      </span>

                        </Button>
                    </div>
                    <div className="s4 lefti">
                        <p className={'bottomBadge'}>حضوری</p>
                    </div>
                    <div className="s4 leftDiv">
                        <Button className={'fz10 blackText'}>
                            اطلاعات تماس
                        </Button>
                    </div>

                </div>

            </Card>
        </Grid>
    )
}
export default PatientCard;
