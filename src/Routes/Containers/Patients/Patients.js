import React, {Component} from 'react';
import './Patients.scss'
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import Loading from "../../../Common/Loading";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import {AgGridReact} from "ag-grid-react";
import PatientCard from "./Components/PatientsCard/patientCard";
import Button from "@material-ui/core/Button";

class PatientsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRadio: 'all',
            headerHeight: 36,
            suppressRowTransform: false,
            singleClickEdit: true,
            rowHeight: 36,
            grid: true,
            loading: true,
            defaultColDef: {
                sortable: true,
                filter: true
            },

            columnDefs: [{
                headerName: "ID", field: "id", sortable: true
            }, {
                headerName: "نام", field: "name", sortable: true
            }, {
                headerName: "نام کاربری", field: "username", sortable: true
            }, {
                headerName: "آدرس", field: "address.street", sortable: true
            }, {
                headerName: "تلفن", field: "phone", sortable: true
            }, {
                headerName: "ایمیل", field: "email", sortable: true
            }]


        };

        this.onGridReady = this.onGridReady.bind(this);
        this.onClicked = this.onClicked.bind(this);
        this.filterPatients = this.filterPatients.bind(this);

    }

    filledData() {
        this.setState(
            {
                loading: false
            }
        );
        setTimeout(() => {
            let toggle = document.getElementById('container');
            let toggleContainer = document.getElementById('toggle-container');
            let toggleNumber;

            toggle.addEventListener('click', function () {
                toggleNumber = !toggleNumber;
                if (toggleNumber) {
                    toggleContainer.style.clipPath = 'inset(0 0 0 50%)';
                    toggleContainer.style.backgroundColor = '#46A9FF';
                } else {
                    toggleContainer.style.clipPath = 'inset(0 50% 0 0)';
                    toggleContainer.style.backgroundColor = '#46A9FF';
                }
                console.log(toggleNumber)
            });

        }, 0)
    };

    componentDidMount() {
        // PatientServices.getPatientsList()
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(result => result.json())
            .then(rowData => this.setState({rowData})).finally(() => {
            this.filledData();

        })
    }

    handleChange = event => {
        this.setState({selectedRadio: event.target.value});
    };

    returnView() {
        let component;
        this.state.grid ? (component = (
            <div className="ag-theme-balham"
                 style={{
                     height: '405px',
                     direction: 'rtl',
                     width: '1200px',
                     position: 'fixed',
                     left: '15px',
                     bottom: '5px',
                 }}>
                <AgGridReact
                    suppressAnimationFrame={false}
                    filter={true}
                    sortable={true}
                    enableRtl={true}
                    animateRows={true}
                    columnDefs={this.state.columnDefs}
                    rowData={this.state.rowData}
                    onGridReady={this.onGridReady}
                >
                </AgGridReact>
            </div>
        )) : component = this.buildGridCards();

        return component;
    }

    buildGridCards() {
        console.log(this.state.rowData);
        let gridShow;
        gridShow = (
            this.state.rowData.map((element, key) => {
                return (
                    <PatientCard data={element} key={key}/>
                )
            })
        );
        return gridShow;
    }

    onClicked() {
        //this.gridApi.getFilterInstance("name").getFrameworkComponentInstance().componentMethod("Hello World!");
    }

    onGridReady(params) {
        this.gridApi = params.api;
    }

    filterPatients(e) {
        // console.log(e.target.value)
        this.gridApi.setQuickFilter(e.target.value);
    }

    render() {

        return (
            this.state.loading ? <Loading/> :
                <div className={'PatientsPage'}>

                    <Grid container direction="row"
                          justify="space-between"
                          alignItems="center" spacing={16}>
                        <Grid item xs={6} sm={6}>
                            <input onChange={this.filterPatients} className={'searchInput'}
                                   type="text" placeholder={'جستجوی بیمار'}/>
                            <Button disableTouchRipple={true} className={'searchButtonForInput'}/>
                        </Grid>
                        <Grid item xs={6} sm={6}>
                            <div id="container" onClick={() => this.setState({
                                grid: !this.state.grid
                            })}>
                                <div className="inner-container">
                                    <div className="toggle">
                                        <p>نمایش کارتی </p>
                                    </div>
                                    <div className="toggle">
                                        <p>نمایش لیستی</p>
                                    </div>
                                </div>
                                <div className="inner-container" id='toggle-container'>
                                    <div className="toggle">
                                        <p>نمایش کارتی</p>
                                    </div>
                                    <div className="toggle">
                                        <p>نمایش لیستی</p>
                                    </div>
                                </div>
                            </div>
                        </Grid>

                    </Grid>

                    <Grid container direction="row"
                          justify="space-between"
                          alignItems="center" spacing={16}>
                        <Grid item xs={6} sm={6}>
                            <Grid item xs={12} sm={12}>
                                <Card>
                                    <Grid item xs={12} sm={12}>
                                        <p className={'displaySelectText'}>نمایش: </p>
                                        <div className={'filterSelector'}>
                                            <FormControl component="fieldset">
                                                <RadioGroup
                                                    aria-label="position"
                                                    name="position"
                                                    value={this.state.selectedRadio}
                                                    onChange={this.handleChange}
                                                    row
                                                >
                                                    <FormControlLabel
                                                        value="all"
                                                        control={<Radio color="primary"/>}
                                                        label="همه"
                                                        labelPlacement="end"
                                                    />
                                                    <FormControlLabel
                                                        value="virtual"
                                                        control={<Radio color="primary"/>}
                                                        label="ویزیت مجازی"
                                                        labelPlacement="end"
                                                    />
                                                    <FormControlLabel
                                                        value="hozoori"
                                                        control={<Radio color="primary"/>}
                                                        label="ویزیت حضوری"
                                                        labelPlacement="end"
                                                    />
                                                    <FormControlLabel
                                                        value="moshavereApp"
                                                        control={<Radio color="primary"/>}
                                                        label="مشاوره اپلیکیشن"
                                                        labelPlacement="end"
                                                    />
                                                </RadioGroup>
                                            </FormControl>

                                        </div>


                                    </Grid>

                                </Card>
                            </Grid>

                        </Grid>
                        <Grid item xs={2} sm={2}>
                            <Card>خروجی اکسل</Card>
                        </Grid>
                        <Grid item xs={4} sm={4}>
                            <Card>تعداد کل بیماران</Card>
                        </Grid>
                    </Grid>

                    <div

                    >

                        <div className={'yScroller'}>
                            <Grid container direction="row"
                                  justify="space-between"
                                  alignItems="center" spacing={16}>
                                <Grid item xs={12} sm={12} className={'rowContainer'}>
                                    {this.returnView()}
                                </Grid>
                            </Grid>
                        </div>
                    </div>

                </div>
        );

    }

}

export default PatientsPage;
