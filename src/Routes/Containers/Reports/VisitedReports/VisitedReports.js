import React from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';
import './VisitedReports.scss'
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";


const VisitedReports = () => {
    const options = {
            colors: ['#46A9FF', '#52FF46', '#FFC146'],
            xAxis: {
                minorTickInterval: 'auto',
                startOnTick: true,
                endOnTick: true,
                tickPixelInterval: 10
            },

            yAxis: {
                tickPixelInterval: 10
            },
            title: {
                text: ''
            }
            ,
            tooltip: {
                backgroundColor: '#46A9FF',
                borderRadius: 15,
                style: {
                    color: 'white',
                    fontWeight:
                        'bold',
                    fontFamily: 'irBold',
                }
                ,
                shared: true,
            },
            series: [

                {

                    data: [1, 2, 1, 4, 3, 6, 7, 3, 8, 6, 9],
                    shadow: true,
                    marker: {
                        enabled: true,
                        radius: 3,
                    },
                    name: ' ویزیت حضوری '
                },
                {
                    data: [1, 9, 17, 4, 13, 16, 17, 13, 8, 16, 19], shadow: true, marker: {
                        enabled: true,
                        radius: 3

                    },
                    name: ' مشاوره در اپلیکیشن ',

                },
                {
                    data: [11, 22, 11, 41, 13, 63, 44, 22, 11, 1, 12], shadow: true, marker: {
                        enabled: true,
                        radius: 3
                    },
                    name: ' ویزیت مجازی '
                }

            ]
        }
    ;
    const ReportsController = () => {
        return (
            <Grid container spacing={8}>
                <Grid item xs={5}>
                    <Grid container spacing={8}>
                        <Grid item xs={7}>
                            <Paper>
                                <span>انتخاب محتوا</span>
                            </Paper>
                        </Grid>
                        <Grid item xs={5}>
                            <Paper>
                                <span>
                                    انتخاب تاریخ
                                </span>
                            </Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper>
                                <p className={'noMargin'}>نمایش ویزیت ها</p>
                                <p>
                                    <span>ویزیت حضوری</span>
                                    <span>مشاوره در اپلیکیشن</span>
                                    <span>ویزیت مجازی</span>
                                </p>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={4}/>
                <Grid item xs={3}>
                    <Grid container spacing={8}>
                        <Grid item xs={12}>
                            <Paper>xs=12</Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Paper>
                                <p className="noMargin">دریافت فایل خروجی</p>
                                <p>دانلود</p>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        )
    };
    return (
        <div>
            <ReportsController/>
            <div className="High">
                <HighchartsReact
                    highcharts={Highcharts}
                    constructorType={'stockChart'}
                    options={options}
                />
            </div>
        </div>
    );

};

export default VisitedReports;
