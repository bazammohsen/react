import React, {useRef, useState} from 'react';
import VisitedGrid from "./Components/VisitedGrid";
import Grid from "@material-ui/core/Grid";
import './VisitedList.scss'
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import PersianDateTimePicker from 'mui-persian-datetime-picker'
import caret from '../../../../assets/icons/caret-down.png'
import moment from "jalali-moment";
import ToPersianNumber from "../../../../Common/Converters/ToPersianNumber";


const VisitedList = (props) => {
    const childRef1 = useRef();
    const childRef2 = useRef();
    const childRef3 = useRef();

    const filterAllChilds = (e) => {
        childRef1.current.filter(e.target.value);
        childRef2.current.filter(e.target.value);
        childRef3.current.filter(e.target.value);
    };
    const [dateState, dateStateChanger] = useState({
        date: moment().format('jYYYY/jM/jD'),
    });
    const changeDate = (event) => {
        let m = moment(event).format('jYYYY/jM/jD');
        dateStateChanger({
            date: m
        });
    };

    function selectDate() {
        document.querySelectorAll('.myDateWrapper button[class*="MuiButtonBase-root"]')[0].click();
    }

    return (
        <div className={'VisitedListsContainer'}>
            <Grid container spacing={24} className={'topBar'}>
                <Grid item xs={12} className={'S12'}>
                    <Grid item xs={2}>
                        <Paper className={'myDateWrapper'}>
                            <span className={'blackText'}>انتخاب تاریخ</span>
                            <span className={'selectDate'} onClick={selectDate} style={{
                                position: 'absolute',
                                marginTop: ' 5px'
                            }}>{ToPersianNumber.NumberConverter(dateState.date)}</span>
                            <span><img onClick={selectDate} className={'caretImage selectDate'} src={caret}
                                       alt=""/></span>
                            <span style={{display: 'none'}}>
      <PersianDateTimePicker onChange={changeDate} name="dateTime" label="My date time" inputMode="datetime"
                             fullWidth/>
                        </span>
                        </Paper>
                    </Grid>
                    <Grid item xs={2}>
                    </Grid>
                    <Grid item xs={6}>
                        <input className={'searchInput'}
                               type="text" onChange={filterAllChilds} placeholder={'جستجوی بیمار'}/>
                        <Button disableTouchRipple={true} className={'searchButtonForInput'}/>
                    </Grid>

                </Grid>
            </Grid>
            <Grid container spacing={24}>
                <Grid item xs={12} className={'S12'}>
                    <Grid item xs={4}>
                        <VisitedGrid ref={childRef1} color={'#46A9FF'} title={'ویزیت حضوری'} mainColor={"blue"}
                                     requestUrl={"https://jsonplaceholder.typicode.com/comments?postId=1"}/>
                    </Grid>
                    <Grid item xs={4}>
                        <VisitedGrid ref={childRef2} color={'#FFC146'} title={'ویزیت مجازی'} mainColor={"yellow"}
                                     requestUrl={"https://jsonplaceholder.typicode.com/comments?postId=1"}/>
                    </Grid>
                    <Grid item xs={4}>
                        <VisitedGrid ref={childRef3} color={'#52FF46'} title={'مشاوره در اپلیکیشن'} mainColor={"green"}
                                     requestUrl={"https://jsonplaceholder.typicode.com/comments?postId=1"}/>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};
export default VisitedList;
