import React, {useEffect, useState, forwardRef, useImperativeHandle} from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import {AgGridReact} from "ag-grid-react";
import './VisitedGrid.scss';
// import localization from '../../../../../Common/AgGridLocalization';

const VisitedGrid = forwardRef((props, ref) => {
    let gridApi;
    const [GridApiState, GridapiChanger] = useState({
        gridApi: {}
    });

    const onGridReady = (params) => {
        gridApi = params.api;
        GridapiChanger({
            gridApi: gridApi
        })
    };

    useImperativeHandle(ref, () => ({
        filter(filter) {
            GridApiState.gridApi.setQuickFilter(filter);
        }
    }));

    const [VisitedListState, VisitedListStateChanger] = useState(
        {
            VisitedListGridState: {
                columnDefs: [],
                rowData: []
            }

        }
    );

    useEffect(() => {
        fetch(props.requestUrl)
            .then(result => result.json())
            .then(rowData => VisitedListStateChanger({
                VisitedListGridState: {
                    columnDefs: [{
                        headerName: "id", field: "postId"
                    }, {
                        headerName: "نام", field: "name"
                    }, {
                        headerName: "قیمت", field: "id"
                    }, {
                        headerName: "توضیحات", field: "body", width: 500
                    }],
                    rowData: rowData,
                }
            }))
    }, []);

    return (
        <div className="VisitedGrid">
            <p style={{textAlign: 'center'}}><span style={{color: props.color}}>
                {props.title}
            </span></p>
            <div
                className={"ag-theme-balham " + props.mainColor}
                style={{
                    height: '435px',
                    width: '95%',
                }}
            >
                <AgGridReact
                    className={props.mainColor}
                    onGridReady={onGridReady}
                    suppressAnimationFrame={false}
                    filter={true}
                    sortable={true}
                    enableRtl={true}
                    animateRows={true}
                    // localeText={{...localization}}
                    columnDefs={VisitedListState.VisitedListGridState.columnDefs}
                    rowData={VisitedListState.VisitedListGridState.rowData}>
                </AgGridReact>
            </div>
        </div>
    )

});
export default VisitedGrid;
