import React, {Component} from "react";
import "./MainPage.scss";
import WhiteContainer from "../../Components/WhiteContainer/WhiteContainer";
import LeftSideBar from "../../Components/LeftSideBar/LeftSideBar";
import PatientsPage from '../Containers/Patients/Patients'
import VisitedReports from '../Containers/Reports/VisitedReports/VisitedReports'
import TopBar from "../../Components/TopBar/TopBar";
import VisitedList from "../Containers/Reports/VisitedList/VisitedList";
import DrugsList from "../Containers/DrugsList/DrugsList";
import {connect} from 'react-redux';

var moment = require('moment-jalaali')
const
    miniRoutes = {
        dashboard: [<div/>, <div/>, <div/>],
        visits: [<div/>, <div/>, <div/>],
        persons: [<PatientsPage/>],
        drugs: [<DrugsList/>],
        chart: [<VisitedReports/>, <VisitedList/>]
    };
const todayJalali = moment().format('jYYYY/jM/jD');

class MainPage extends Component {


    constructor(props, history) {
        super(props);
        this.changePage = this.changePage.bind(this);
    }


    state = {
        currentPage: {
            title: 'صفحه اصلی',
            body: <div/>
        },
        topBarData: {
            nextVizits: 10,
            timeToNext: 0,
            today: todayJalali,
            Notifications: [1],
            Messages: [1, 2]
        }
    };

    changePage(content) {
        const elements = document.getElementsByClassName('whiteText');
        elements.length > 0 ? elements[0].classList.remove('active') : console.log('first');
        let whole = localStorage.selectedButton;
        let index = whole.match(/\d+/g).map(Number)[0];
        let topRoute = localStorage.selectedButton.split(index + '')[0];
        this.setState(
            {
                currentPage: {
                    title: content,
                    body: miniRoutes[topRoute + ''][index]
                }
            });

        this.props.OnPageChange(content);
        this.renderBlueSideButton()

    }


    updateWhiteContainerBody() {
        let body = this.state.currentPage.body;
        this.setState({currentPage: {body: <div/>}});
        setTimeout(() => {
            this.setState({currentPage: {body: body}})
        }, 0);
        this.renderBlueSideButton()
    }

    renderBlueSideButton() {
        setTimeout(() => {
            let selectedButton = document.getElementById(localStorage.selectedButton);
            selectedButton.classList.add('active');

        }, 0)
    }

    render() {
        return (
            <div className="mainPage">
                <div className="blackWrapper">
                    <TopBar {...this.props} data={this.state.topBarData}/>
                    <LeftSideBar setPage={this.changePage}/>
                    <div className="whiteContainer">
                        <WhiteContainer reload={() => this.updateWhiteContainerBody()}
                                        title={this.props.CurrentPage}
                                        buttonTitle="بروز رسانی" body={this.state.currentPage.body}/>
                    </div>
                </div>
            </div>
        );
    }
}

const MapStateToProps = state => {
    return {
        CurrentPage:
        state.CurrentPage
    };
};
const MapDispatchToProps = dispatch => {
    return {
        OnPageChange: (page) => {
            return dispatch({
                type: 'CHANGEPAGE', value: page
            })
        },
        RefreshPage: () => dispatch({type: 'REFRESHCURRENTPAGE',})
        // CurrentPage:
        // state.CurrentPage
    };
};

export default connect(MapStateToProps, MapDispatchToProps)(MainPage);
