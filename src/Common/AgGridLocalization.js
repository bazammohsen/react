const localization = () => {
    return (
        {
            // for filter panel
            page: 'daPage',
            more: 'daMore',
            to: 'daTo',
            of: 'daOf',
            next: 'daNexten',
            last: 'daLasten',
            first: 'daFirsten',
            previous: 'daPreviousen',
            loadingOoo: 'daLoading...',

            // for set filter
            selectAll: 'daSelect Allen',
            searchOoo: 'daSearch...',
            blanks: 'daBlanc',

            // for number filter and text filter
            filterOoo: 'daFilter...',
            applyFilter: 'daApplyFilter...',
            equals: 'daEquals',
            notEqual: 'daNotEqual',

            // for number filter
            lessThan: 'daLessThan',
            greaterThan: 'daGreaterThan',
            lessThanOrEqual: 'daLessThanOrEqual',
            greaterThanOrEqual: 'daGreaterThanOrEqual',
            inRange: 'daInRange',

            // for text filter
            contains: 'daContains',
            notContains: 'daNotContains',
            startsWith: 'daStarts dawith',
            endsWith: 'daEnds dawith',

            // filter conditions
            andCondition: 'daAND',
            orCondition: 'daOR',

            // the header of the default group column
            group: 'laGroup',

            // tool panel
            columns: 'laColumns',
            filters: 'laFilters',
            rowGroupColumns: 'laPivot Cols',
            rowGroupColumnsEmptyMessage: 'la drag cols to group',
            valueColumns: 'laValue Cols',
            pivotMode: 'laPivot-Mode',
            groups: 'laGroups',
            values: 'laValues',
            pivots: 'laPivots',
            valueColumnsEmptyMessage: 'la drag cols to aggregate',
            pivotColumnsEmptyMessage: 'la drag here to pivot',
            toolPanelButton: 'la tool panel',

            // other
            noRowsToShow: 'la no rows',

            // enterprise menu
            pinColumn: 'laPin Column',
            valueAggregation: 'laValue Agg',
            autosizeThiscolumn: 'laAutosize Diz',
            autosizeAllColumns: 'laAutsoie em All',
            groupBy: 'laGroup by',
            ungroupBy: 'laUnGroup by',
            resetColumns: 'laReset Those Cols',
            expandAll: 'laOpen-em-up',
            collapseAll: 'laClose-em-up',
            toolPanel: 'laTool Panelo',
            export: 'laExporto',
            csvExport: 'la CSV Exportp',
            excelExport: 'la Excel Exporto (.xlsx)',
            excelXmlExport: 'la Excel Exporto (.xml)',

            // enterprise menu pinning
            pinLeft: 'laPin &lt;&lt;',
            pinRight: 'laPin &gt;&gt;',
            noPin: 'laDontPin &lt;&gt;',

            // enterprise menu aggregation and status bar
            sum: 'laSum',
            min: 'laMin',
            max: 'laMax',
            none: 'laNone',
            count: 'laCount',
            average: 'laAverage',

            // standard menu
            copy: 'laCopy',
            copyWithHeaders: 'laCopy Wit hHeaders',
            ctrlC: 'ctrl n C',
            paste: 'laPaste',
            ctrlV: 'ctrl n V'
        }
    );
}
export default localization;
