import React, {Fragment} from 'react'
import './Loading.scss'
const Loading = ()=>{
    return(
        <Fragment>
            <div id="loading_animation">
                <div className="middle_ring"></div>
                <div className="inner_ring"></div>
                <span id="animation_message">meh</span>
            </div>
        </Fragment>
    )
}
export default Loading;