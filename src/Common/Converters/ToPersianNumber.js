class ToPersianNumber {


    static NumberConverter(string) {
        let St = string + '';
        let reAranged = [];
        const pNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        for (let i = 0; i < St.length; i++) {
            reAranged.push(St[i]);
            for (let j = 0; j < 10; j++) {

                if (Number(St[i]) === j) {
                    reAranged[i] = pNumbers[j];
                }
            }

        }
        return reAranged;
    }
}

export default ToPersianNumber;
