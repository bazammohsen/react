const initialState = {CurrentPage: 'صفحه اصلی'};
const Reducer = (state = initialState, action) => {
    let css = "font-size: 13px;font-weight:700;text-shadow: -1px 2px 6px rgba(66, 200, 142, 0.64);color:green;";

    console.log('%cfrom Reducer', css, action);
    // console.log(action);

    switch (action.type) {
        ///////
        case 'CHANGEPAGE':
            const newState = Object.assign({}, state);
            newState.CurrentPage = action.value
            return newState;
        //////////
        default:
            return state;
    }
};
export default Reducer;
